//
//  DisclaimerVC.swift
//  TotemLyb
//
//  Created by Vladislav Mikheenko on 2/14/19.
//  Copyright © 2019 Vladislav Mikheenko. All rights reserved.
//

import UIKit
import Foundation

import SwiftyJSON

class DisclaimerVC: UIViewController {
    
    @IBOutlet weak var textView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.global().async {
            if Reachability.isConnectedToNetwork() {
                self.fetchDisclaimer()
            }
        }
        
    }
    
    func fetchDisclaimer(){
        
        DispatchQueue.global().async {
            let session = URLSession.shared
            let url2 = globalLink + "sync/get-disclaimer"
            
            let request2 = URLRequest(url: URL(string: url2)!)
            
            let task = session.dataTask(with: request2, completionHandler: { data, response, error in
                
                guard error == nil else {
                    print("error is \(error!.localizedDescription)")
                    return
                }
                
                guard data != nil else {
                    return
                }
                
                let json = JSON(data!)
                let strUtf = json["data"].stringValue
                let htmlData = NSString(string: strUtf).data(using: String.Encoding.unicode.rawValue)
                let options = [NSAttributedString.DocumentReadingOptionKey.documentType:
                    NSAttributedString.DocumentType.html]
                let attributedString = try? NSMutableAttributedString(data: htmlData ?? Data(), options: options, documentAttributes: nil)
                
                DispatchQueue.main.async {
                    self.textView.attributedText = attributedString
                }
            })
            
            task.resume()
        }
        }
        
        @IBAction func indietro(_ sender: Any) {
            self.navigationController?.popViewController(animated: true)
        }
}
