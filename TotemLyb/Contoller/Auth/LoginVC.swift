//
//  LoginVC.swift
//  TotemLyb
//
//  Created by Vladislav Mikheenko on 2/12/19.
//  Copyright © 2019 Vladislav Mikheenko. All rights reserved.


import UIKit

import SwiftyJSON

let globalLink = "http://testapp.lyb.it:45887/"

class LoginVC: UIViewController {
    
    
    var from = ""
    
    var registered = false
    
    @IBOutlet weak var emailLbl: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailLbl.autocorrectionType = .no
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkActive(vc: self)
    }
    
    
    //checkActive
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        checkActive(vc: self)
    }
    
    
    @IBAction func avanti(_ sender: Any) {
        print("work")
        if let text = emailLbl.text {
            print("work2")
            if text.isEmpty {
                showAlert(withTitle: "Attenzione", withText: "il campo del e-mail deve essere compilato", vc: self)
            } else if (text.contains("@") == false) && (text.contains(".") == false)  && (text.count < 3)  {
                showAlert(withTitle: "Attenzione", withText: "Inserire una email valida", vc: self)
            } else {
                //user mail default
                user.email = text
                let session = URLSession.shared
                if from != "" {
                    //
                    
                    if from == "login" {
                        //MARK: -login
                        
                        
                        DispatchQueue.global().async {
                            let url = URL(string: globalLink + "sync/verify-email")!
//                            let url = URL(string: "http://chimar-torino-api.lifeyourbrand.it/sync/verify-email")!
                            
                            var request = URLRequest(url: url)
                            
                            request.httpMethod = "POST"
                            request.httpBody = "email=\(text)".data(using: .utf8)
                            
                            let task = session.dataTask(with: request, completionHandler: { data, response, error in
                                print("work3 \(response)")
                                
                                guard error == nil else {
                                    print("error is \(error?.localizedDescription)")
                                    return
                                }
                                
                                guard data != nil else {
                                    print("data error")
                                    return
                                }
                                
                                print("work44")
                                
                                let json = JSON(data!)
                                print("work55 \(json)")
                                //test error
                                guard json["Total"].intValue > 0 else {
                                    DispatchQueue.main.async {
                                        self.performSegue(withIdentifier: "regisetr", sender: nil)
                                        user.register = false
                                    }
                                    return
                                }
                                
                                user.name = json["data"].dictionaryValue.values.first?.stringValue
                                user.register = true
                                user.id = json["data"].dictionaryValue.keys.first
                                
                                DispatchQueue.global().async {
                                    let url2 = globalLink + "sync/login"
                                    
                                    var request2 = URLRequest(url: URL(string: url2)!)
                                    
                                    request2.httpMethod = "POST"
                                    request2.httpBody = "id=\(user.id!)".data(using: .utf8)
                                    
                                    
                                    let task = session.dataTask(with: request2, completionHandler: { data, response, error in
                                        
                                        guard error == nil else {
                                            print("error is \(error?.localizedDescription)")
                                            return
                                        }
                                        
                                        guard data != nil else {
                                            return
                                        }
                                        
                                        let json2 = JSON(data!)
                                        print("di2")
                                        print(json2)
                                        
                                        let dataJson = json2["next"].stringValue
                                        
                                        guard !dataJson.isEmpty else {
                                            DispatchQueue.main.async {
                                                showAlert(withTitle: "Attenzione", withText: "Errore del server", vc: self)
                                            }
                                            return
                                        }
                                        
                                        DispatchQueue.main.async {
                                            if dataJson == "entra" || dataJson == "previsione" {
                                                self.performSegue(withIdentifier: "logOut", sender: nil)
                                            } else if dataJson == "esci" {
                                                showAlert(withTitle: "Attenzione", withText: "Sei già loggato", vc: self)
                                            } else if json["Stato"].stringValue == "no" {
                                                showAlert(withTitle: "Attenzione", withText: "è già presente un QR per oggi", vc: self)
                                            }
                                        }
                                        
                                    })
                                    task.resume()
                                }
                            })
                            
                            task.resume()
                        }
                        
                        
                        
                        
                    } else {
                        print("work4")
                        //MARK: -logout
                        
                        DispatchQueue.global().async {
                            let url = globalLink + "sync/verify-email"
                            let session = URLSession.shared
                            var request = URLRequest(url: URL(string: url)!)
                            
                            request.httpMethod = "POST"
                            request.httpBody = "email=\(text)".data(using: .utf8)
                            
                            let task = session.dataTask(with: request, completionHandler: { data, response, error in
                                
                                guard error == nil else {
                                    print("error is \(error?.localizedDescription)")
                                    return
                                }
                                
                                guard data != nil else {
                                    return
                                }
                                let json = JSON(data!)
                                print(json)
                                print("do2 id")
                                
                                guard json["Total"].intValue > 0 else {
                                    DispatchQueue.main.async {
                                        showAlert(withTitle: "Attenzione", withText: "Non puoi uscire senza essere entrato", vc: self)
                                    }
                                    user.register = false
                                    return
                                }
                                
                                user.name = json["data"].dictionary?.values.first?.stringValue
                                user.register = true
                                user.id = json["data"].dictionaryValue.keys.first
                                
                                DispatchQueue.global().async {
                                    let url = "http://testapp.lyb.it:45887/sync/login"
                                    
                                    var request = URLRequest(url: URL(string: url)!)
                                    
                                    request.httpMethod = "POST"
                                    request.httpBody = "id=\(user.id ?? "0")".data(using: .utf8)
                                    
                                    let task2 = session.dataTask(with: request, completionHandler: { (data, response, error) in
                                        
                                        guard error == nil else {
                                            print("error is \(error?.localizedDescription)")
                                            return
                                        }
                                        
                                        guard data != nil else {
                                            return
                                        }
                                        
                                        let json = JSON(data!)
                                        print("login2")
                                        print(json)
                                        
                                        guard !json["next"].stringValue.isEmpty else {
                                            DispatchQueue.main.async {
                                                showAlert(withTitle: "Attenzione", withText: "Errore del server", vc: self)
                                            }
                                            return
                                        }
                                        
                                        DispatchQueue.main.async {
                                            if json["next"].stringValue == "entra" {
                                                showAlert(withTitle: "Attenzione", withText: "Azione non permessa", vc: self)
                                            } else if json["next"].stringValue == "esci" {
                                                self.performSegue(withIdentifier: "logOut", sender: nil)
                                            } else {
                                                showAlert(withTitle: "Attenzione", withText: "è già presente un QR per oggi", vc: self)
                                            }
                                        }
                                    })
                                    
                                    task2.resume()
                                }
                                
                            })
                            
                            task.resume()
                        }
                    }
                    
                    //check email in site
                    
                    //                    checkEmail(text: text)
                }
            }
        } else {
            showAlert(withTitle: "Attenzione", withText: "il campo del e-mail deve essere compilato", vc: self)
        }
        //segue
        checkActive(vc: self)
    }
    
    @IBAction func qrTap(_ sender: Any) {
        checkActive(vc: self)
    }
    
    //MARK: -Methods
    
}

extension LoginVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        checkActive(vc: self)
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true;
    }
}
