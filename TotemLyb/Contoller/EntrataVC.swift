//
//  ViewController.swift
//  TotemLyb
//
//  Created by Vladislav Mikheenko on 2/12/19.
//  Copyright © 2019 Vladislav Mikheenko. All rights reserved.
//

import UIKit

var timer = Timer()


//check active function
public func checkActive(vc: UIViewController) {
    //test
    timer.invalidate()
    timer = Timer.scheduledTimer(withTimeInterval: 45, repeats: false, block: { (Timer) in
        vc.navigationController?.popViewController(animated: true)
    })
}

//show simple alert

func showAlert(withTitle title: String ,withText text: String, vc: UIViewController) {
    
    let alert = UIAlertController(title: title, message: text, preferredStyle: .alert)
    
    let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
    
    alert.addAction(action)
    
    vc.present(alert, animated: true, completion: nil)
}

class EntrataVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !Reachability.isConnectedToNetwork() {
            //alert
            let alert = UIAlertController(title: "Attenzione", message: "Internet non disponibile", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func logTap(_ sender: UIButton) {
        var segueIdentifier = ""
        if sender.tag == 1 {
            segueIdentifier = "loginSegue"
        } else {
            segueIdentifier = "logoutSegue"
        }
        
        performSegue(withIdentifier: segueIdentifier, sender: segueIdentifier)
        
        checkActive(vc: self)
    }

    //Mark: - Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let vc = segue.destination as! LoginVC
        
        if segue.identifier == "loginSegue" {
            vc.from = "login"
        } else if segue.identifier == "logoutSegue" {
            vc.from = "logout"
        }
    }
    
}
