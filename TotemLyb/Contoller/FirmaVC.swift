//
//  FirmaVC.swift
//  TotemLyb
//
//  Created by Vladislav Mikheenko on 2/12/19.
//  Copyright © 2019 Vladislav Mikheenko. All rights reserved.
//

import UIKit
import SwiftyJSON

class FirmaVC: UIViewController {
    
    var timer = Timer()
    
    
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var imgView: UIImageView!
    
    var lastPoint = CGPoint.zero
    var swiped = false
    
    var imageData: Data?
    
    var parameters: [String : String]?
    
    var imageName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        print("codise \(user.id)")
//        print("ref \(user.refer)")
//        print("email \(user.email)")
//        print("register \(user.register)")
//        print("society \(user.society)")
//        print("surname \(user.surname)")
        
        activity.isHidden = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.activity.isHidden = true
        self.activity.stopAnimating()
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        checkActive(vc: self)
        
        swiped = false
        
        if let touch = touches.first {
            lastPoint = touch.location(in: self.view)
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        checkActive(vc: self)
        
        swiped = true
        
        if let touch = touches.first {
            let currentPoint = touch.location(in: view)
            drawLine(lastPoint, toPoint: currentPoint)
            
            lastPoint = currentPoint
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        checkActive(vc: self)
        
        if !swiped {
            drawLine(lastPoint, toPoint: lastPoint)
        }
    }
    
    func drawLine(_ fromPoint: CGPoint, toPoint: CGPoint) {
        UIGraphicsBeginImageContext(view.frame.size)
        let context = UIGraphicsGetCurrentContext()
        
        imgView.image?.draw(in: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height))
        
        context?.move(to: CGPoint(x: fromPoint.x, y: fromPoint.y))
        context?.addLine(to: CGPoint(x: toPoint.x, y: toPoint.y))
        
        context?.setLineCap(CGLineCap.round)
        context?.setLineWidth(3)
        context?.setStrokeColor(UIColor.black.cgColor)
        context?.setBlendMode(CGBlendMode.normal)
        
        context?.strokePath()
        
        imgView.image = UIGraphicsGetImageFromCurrentImageContext()
        
        let saveImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        //data
        if let image = saveImage?.jpegData(compressionQuality: 0.8) {
            imageData = image
        }
    }
    
    
    //Grazie back to login view
    @objc func cancel() {
        print("done")
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func register() {
        //register
        DispatchQueue.global().async {
            let url = URL(string: "http://testapp.lyb.it:45887/sync/registrazione")!
            var request = URLRequest(url: url)
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            let postString = "nome=\(user.name ?? "none")&cognome=\(user.surname ?? "none")&email=\(user.email ?? "none")&societa=\(user.society ?? "none")&referente=\(user.refer ?? 0)&inviomail=NO"
            
            print("no")
            print(postString)
            
            request.httpBody = postString.data(using: .utf8)
            
            // Getting response for POST Method
            DispatchQueue.main.async {
                let task = URLSession.shared.dataTask(with: request) { data, response, error in
                    guard let data = data, error == nil else {
                        return // check for fundamental networking error
                    }
                    
                    let json = try? JSON(data: data)
                    
                    print("json \(String(describing: json))")
                    
                    //check if json is nil
                    
                    
                    
                    if json?["Stato"].stringValue == "ok" {
                        user.id = json?["Visitatore"].stringValue
                        self.sendPhotoMail()
                    } else {
                        print("wronh json")
                    }
                }
                task.resume()
            }
        }
        
    }
    
    func sendPhotoMail() {
        //def values

        let dateFormater = DateFormatter()

        dateFormater.dateFormat = "yyyy-MM-dd_HH-mm-ss"

        let date = Date()
        let curDate = dateFormater.string(from: date)

        imageName = "\(user.id!)__\(curDate).jpg"

        var request = URLRequest(url: URL(string: "http://chimar-torino-api.lifeyourbrand.it/sync/upload-photo?id=\(user.id!)")!)

        request.httpMethod = "POST"

        let boundary = "---------------------------147378098314664998827445283458"
        let contentType = "multipart/form-data; boundary=\(boundary)"
        request.addValue(contentType, forHTTPHeaderField: "Content-Type")
        var postbody = Data()
        if let data = "\r\n--\(boundary)\r\n".data(using: .utf8) {
            postbody.append(data)
        }
        if let data = "Content-Disposition: form-data; name=\"userfile\"; filename=\"\(imageName)\"\r\n".data(using: .utf8) {
            postbody.append(data)
        }
        if let data = "Content-Type: application/octet-stream\r\n\r\n".data(using: .utf8) {
            postbody.append(data)
        }
        postbody.append(imageData!)
        if let data = "\r\n--\(boundary)--\r\n".data(using: .utf8) {
            postbody.append(data)
        }
        request.httpBody = postbody


//        let boundry = "---------------------------147378098314664998827445283458"
//        let contentType = "multipart/form-data; boundary=\(boundry)"
//
//        var request = URLRequest(url: URL(string: "http://chimar-torino-api.lifeyourbrand.it/sync/upload-photo?id=\(user.id!)")!)
//
//        request.httpMethod = "POST"
//        request.addValue(contentType, forHTTPHeaderField: "Content-Type")
//
//        var postBody = Data()
//        postBody.append("\r\n--\(boundry)\r\n".data(using: .utf8)!)
//        postBody.append("Content-Disposition: form-data; name=\"userfile\"; filename=\"\(imageName)\"\r\n".data(using: .utf8)!)
//        postBody.append("Content-Type: application/octet-stream\r\n\r\n".data(using: .utf8)!)
//        postBody.append(imageData!)
//        postBody.append("\r\n--\(boundry)--\r\n".data(using: .utf8)!)
//
//        request.httpBody = postBody

        DispatchQueue.global().async {
            let session = URLSession.shared

            let task = session.dataTask(with: request, completionHandler: { data, response, error in

                guard error == nil else {
                    print("error is \(error?.localizedDescription)")
                    return
                }

                guard data != nil else {
                    return
                }
                let json = JSON(data!)
                print("poro \(json)")

                if json["Stato"].stringValue == "ok" {
                    user.type = "entra"
                    self.entrataUscita(type: user.type!)
                } else {
//                    fatalError()
                }

            })

            task.resume()
        }
        
    }
    
    func entrataUscita(type: String) {
        
        var request = URLRequest(url: URL(string: globalLink + "sync/ingresso")!)
        
        request.httpMethod = "POST"
        
        let authStr = "codice=\(user.id!)&tipo=\(user.type ?? "entra")&codiceVis=null&ipad=\(UIDevice .current)&referente=\(user.refer!)&firma=\(imageName)"
        
        request.httpBody = authStr.data(using: .utf8)
        print("auth String \(authStr)")
        
        
        DispatchQueue.global().async {
            let session = URLSession.shared
            
            let task = session.dataTask(with: request, completionHandler: { data, response, error in
                
                guard error == nil else {
                    print("error is \(error?.localizedDescription)")
                    DispatchQueue.main.async {
                        showAlert(withTitle: "Error", withText: "Request is failed", vc: self)
                        self.activity.isHidden = true
                        self.activity.stopAnimating()
                    }
                    return
                }
                
                guard data != nil else {
                    return
                }
                let json = JSON(data!)
                print("poro2 \(json)")
                
                guard  json[].stringValue == "200" else {
                    DispatchQueue.main.async {
                        showAlert(withTitle: "Error", withText: "Request is failed", vc: self)
                        self.activity.isHidden = true
                        self.activity.stopAnimating()
                    }
                    return
                }
                
                print("auth is done")
                
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "grazie", sender: nil)
                    self.activity.isHidden = true
                    self.activity.stopAnimating()
                }
            })
            
            task.resume()
        }

    }
    
    
    @IBAction func inviaTap(_ sender: Any) {
        //test
        self.performSegue(withIdentifier: "grazie", sender: nil)
        //test delete id
        if Reachability.isConnectedToNetwork() {
            self.activity.isHidden = false
            self.activity.startAnimating()
            
            if user.register {
                sendPhotoMail()
            } else {
                register()
            }
        } else {
            showAlert(withTitle: "Error", withText: "Connection", vc: self)
            self.activity.isHidden = true
            self.activity.stopAnimating()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "grazie":
            timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(cancel), userInfo: nil, repeats: false)
        default:
            break
        }
    }
    
    
    @IBAction func cancella(_ sender: Any) {
        imgView.image = nil
    }
    
    @IBAction func indietro(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
