//
//  logOutCell.swift
//  TotemLyb
//
//  Created by Vladislav Mikheenko on 2/13/19.
//  Copyright © 2019 Vladislav Mikheenko. All rights reserved.
//

import UIKit

class LogOutCell: UITableViewCell {

    @IBOutlet weak var nameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
