//
//  logOut.swift
//  TotemLyb
//
//  Created by Vladislav Mikheenko on 2/13/19.
//  Copyright © 2019 Vladislav Mikheenko. All rights reserved.
//

import UIKit

import SwiftyJSON

class LogOutCheck: UIViewController {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        checkActive(vc: self)
    }
    
    
    
    
    @IBAction func siTap(_ sender: Any) {
        checkActive(vc: self)
        
        getLogOut()
    }
    
    @IBAction func indietro(_ sender: Any) {
        checkActive(vc: self)
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: -Methods
    func getLogOut() {
        print("pprp")
        let session = URLSession.shared
        DispatchQueue.global().async {
            let url = URL(string: globalLink + "sync/login")!
            
            var request = URLRequest(url: url)
            
            request.httpMethod = "POST"
            request.httpBody = "id=\(user.id!)".data(using: .utf8)
            
            let task = session.dataTask(with: request, completionHandler: { data, response, error in
                print("pprp op \(response)")
                guard error == nil else {
                    print("error iso \(error?.localizedDescription)")
                    return
                }
                
                guard data != nil else {
                    DispatchQueue.main.async {
                        showAlert(withTitle: "Warning", withText: "Email is wrong", vc: self)
                    }
                    return
                }
                
                let json = JSON(data!)
                
                guard json["Stato"].stringValue == "ok" else {
                    DispatchQueue.main.async {
                    showAlert(withTitle: "Warning2", withText: "Email is wrong", vc: self)
                    }
                    return
                }
                
                user.codice = json["codice"].stringValue
                
                if json["next"].stringValue == "esci" {
                    
                    user.type = "esci"
                    
                    //func from objc version
                    self.entrataUscita(type: user.type!)
                    
                } else if json["next"].stringValue == "previsione" {
                    
                    user.type = "previsione"
                    
                    DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "login1", sender: nil)
                    }
                    
                } else {
                    
                    user.type = ""
                    DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "login1", sender: nil)
                    }
                }
                
            })
            
            task.resume()
        }
    }
    
    //Grazie back to login view
    @objc func cancel() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        switch segue.identifier {
        case "exit":
            timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(cancel), userInfo: nil, repeats: false)
        case "login1":
            print("login1")
        case "reg":
            print("reg")
        default:
            break
        }
    }
    
    func entrataUscita(type: String) {
        
        DispatchQueue.global().async {
            let session = URLSession.shared
            let url = URL(string: globalLink + "sync/ingresso")!
            
            var request = URLRequest(url: url)
            
            request.httpMethod = "POST"
            
            //need use userDef
            let aziendaId = "2"
            
            let authStr = "codice=\(user.id!)&tipo=\(user.type ?? "entra")&codiceVis=\(user.codice!)&ipad=\(UIDevice .current)&referente=nil&id_azienda=\(aziendaId)"
            
            request.httpBody = authStr.data(using: .utf8)
            
            let task = session.dataTask(with: request, completionHandler: { data, response, error in
                
                guard data != nil else {
                    return
                }
                
                print("wroror")
                
                let json = JSON(data!)
                
                if json[].stringValue == "200" {
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "exit", sender: nil)
                    }
                } else {
                    DispatchQueue.main.async {
                        showAlert(withTitle: "Warning", withText: "There was an unexpected error on the server", vc: self)
                    }
                }
            })
            
            task.resume()
        
        }
        
    }
    
}


extension LogOutCheck: UITableViewDelegate, UITableViewDataSource  {
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellLogOut", for: indexPath) as! LogOutCell
        
        if user.name != nil {
            cell.nameLbl.text = user.name
        }
        
        return cell
    }
    
}
