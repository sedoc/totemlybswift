//
//  RefVC.swift
//  TotemLyb
//
//  Created by Vladislav Mikheenko on 2/15/19.
//  Copyright © 2019 Vladislav Mikheenko. All rights reserved.
//

import UIKit

import SwiftyJSON

class RefVC: UIViewController {
    
    var refer = [String]()
    
    @IBOutlet weak var reflbl: UITextField!
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        getRefer()
        
    }
    
    
    
    //MARK: -Methods
    func getRefer() {
        
        
        DispatchQueue.global().async {
            let session = URLSession.shared
            let url = URL(string: globalLink + "sync/ref")!
            
            var request = URLRequest(url: url)
            
            request.httpMethod = "POST"
            
            let task = session.dataTask(with: request, completionHandler: { data, response, error in
                print("pprp")
                guard data != nil else {
                    return
                }
                
                let json = JSON(data!)
                
                json["Referente"].dictionaryValue.values.forEach({ (value) in
                    self.refer.append(value.stringValue)
                })
                
                
                self.refer.sort()
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            })
            
            task.resume()
        }

        
    }
    
    @IBAction func avanti(_ sender: Any) {
        if reflbl.text != "" {
            if refer.contains(reflbl.text ?? "") {
                print("drop")
                if reflbl.text != nil {
                    let index = refer.firstIndex(of: reflbl.text ?? "false")
                    print("index: \(String(describing: index))")
                    
                    if refer.contains(reflbl.text ?? "false") {
                        print("yep")
                      
                        let indexes = refer.enumerated().filter {
                            $0.element.contains(reflbl.text ?? "false")
                            }.map{$0.offset}
                        
                        user.refer = (indexes.first ?? 0) + 1
                    }
                    
                    
                }
                
                
                
                performSegue(withIdentifier: "login2", sender: nil)
                
                
                
            }
        }
    }
    
    @IBAction func indietro(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

//MARK: -TableView delegate & datasourse
extension RefVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return refer.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellRef", for: indexPath)
        
        cell.textLabel?.text = refer[indexPath.row]
        cell.textLabel?.textAlignment = .center
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //refer add
        user.refer = indexPath.row + 1
        
        let cell = tableView.cellForRow(at: indexPath)
        reflbl.text = cell?.textLabel?.text
    }
}

//MARK
