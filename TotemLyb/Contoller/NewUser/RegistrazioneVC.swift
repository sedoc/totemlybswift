//
//  RegistrazioneVC.swift
//  TotemLyb
//
//  Created by Vladislav Mikheenko on 2/12/19.
//  Copyright © 2019 Vladislav Mikheenko. All rights reserved.
//

import UIKit

import SwiftyJSON

class RegistrazioneVC: UIViewController {
    
    @IBOutlet weak var referView: UIView!
    
    @IBOutlet weak var nameLbl: UITextField!
    @IBOutlet weak var surNameLbl: UITextField!
    @IBOutlet weak var emailLbl: UITextField!
    @IBOutlet weak var societyLbl: UITextField!
    @IBOutlet weak var referPick: UIPickerView!
    
    var email = ""
    var reference = [String]()
    var refer = "none"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        referView.layer.cornerRadius = 5
        referView.layer.borderColor = UIColor.black.cgColor
        referView.layer.borderWidth = 2
        
        if user.email != nil {
            emailLbl.text = user.email
        }
        
        getRefer()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        checkActive(vc: self)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        checkActive(vc: self)
    }
    
    @IBAction func indietro(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func avanti(_ sender: Any) {
        
        //need check registred email
        
        var message = ""
        
        print(user.refer)
        
        if nameLbl.text?.isEmpty ?? true {
            
            print("name is empty")
            message = "il campo del nome deve essere compilato"
            showAlert(withTitle: "Attenzione", withText: message, vc: self)
            
        } else if surNameLbl.text?.isEmpty ?? true {
            
            print("surname is empty")
            message = "il campo del cognome deve essere compilato"
            showAlert(withTitle: "Attenzione", withText: message, vc: self)
            
        } else if emailLbl.text?.isEmpty ?? true {
            
            print("email is empty")
            message = "il campo del e-mail deve essere compilato"
            showAlert(withTitle: "Attenzione", withText: message, vc: self)
            
        } else if (emailLbl.text?.contains("@") == false) && (emailLbl.text?.contains(".") == false)  && (emailLbl.text?.count ?? 0 < 3){
            
            print("valid email pls")
            message = "il campo del nome deve essere compilato"
            showAlert(withTitle: "Attenzione", withText: message, vc: self)
            
        } else if societyLbl.text?.isEmpty ?? true {
            
            print("society is empy")
            message = "il campo del società deve essere compilato"
            showAlert(withTitle: "Attenzione", withText: message, vc: self)
            
        } else if user.refer == 0 {
            
            print("refer is empy")
            message = "il campo del referente deve essere compilato"
            showAlert(withTitle: "Attenzione", withText: message, vc: self)
            
        } else {
            print("work2333")
            //            let url = "http://testapp.lyb.it:45887/sync/mail"
            
            //            let param = ["email" : "\(emailLbl.text ?? "")"]
            
            user.name = nameLbl.text
            user.surname = surNameLbl.text
            user.society = societyLbl.text
            
            DispatchQueue.global().async {
                let session = URLSession.shared
                let url = URL(string: globalLink + "sync/verify-email")!
                //                            let url = URL(string: "http://chimar-torino-api.lifeyourbrand.it/sync/verify-email")!
                
                var request = URLRequest(url: url)
                
                request.httpMethod = "GET"
                request.httpBody?.append("email:\(self.emailLbl.text!)".data(using: .utf8)!)
                
                let task = session.dataTask(with: request, completionHandler: { data, response, error in
                    print("work3 \(response)")
                    
                    guard error == nil else {
                        print("error is \(error?.localizedDescription)")
                        return
                    }
                    
                    guard data != nil else {
                        print("data error")
                        return
                    }
                    
                    print("work44")
                    
                    if data != nil {
                        
                        let json = JSON(data!)
                        
                        print("json \(json)")
                        
                        if json["Total"].stringValue == "0" {
                            DispatchQueue.main.async {
                                self.performSegue(withIdentifier: "showDiscl", sender: nil)
                            }
                        } else {
                            DispatchQueue.main.async {
                                showAlert(withTitle: "Attenzione", withText: "Email già registrata", vc: self)
                            }
                        }
                        //                        if result == "Non esiste" {
                        //                            self.performSegue(withIdentifier: "showDiscl", sender: nil)
                        //                        } else {
                        //                            DispatchQueue.main.async {
                        //                                showAlert(withTitle: "Attenzione", withText: "Email già registrata", vc: self)
                        //                            }
                        //                        }
                    } else {
                        DispatchQueue.main.async {
                            showAlert(withTitle: "Attenzione", withText: "Inserire una email valida", vc: self)
                        }
                    }
                    
                })
                
                task.resume()
            }
            
        }
    }
    
    //MARK: -Methods
    func getRefer() {
        
        
        DispatchQueue.global().async {
            let session = URLSession.shared
            let url = URL(string: globalLink + "sync/ref")!
            
            var request = URLRequest(url: url)
            
            request.httpMethod = "POST"
            
            let task = session.dataTask(with: request, completionHandler: { data, response, error in
                print("pprp")
                guard data != nil else {
                    return
                }
                
                let json = JSON(data!)
                
                json["Referente"].dictionaryValue.values.forEach({ (value) in
                    self.reference.append(value.stringValue)
                })
                
                
                self.reference.sort()
                
                DispatchQueue.main.async {
                    self.referPick.reloadAllComponents()
                }
            })
            
            task.resume()
        }
        
        
    }
    
}

//MARK: -picker
extension RegistrazioneVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return reference.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return reference[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        user.refer = row
        print("user ref \(user.refer)")
    }
}

extension RegistrazioneVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        checkActive(vc: self)
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true;
    }
}
