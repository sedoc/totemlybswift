//
//  User.swift
//  TotemLyb
//
//  Created by Vladislav Mikheenko on 2/14/19.
//  Copyright © 2019 Vladislav Mikheenko. All rights reserved.
//

import UIKit

var user = User()

class User{
    var name: String? {
        didSet {
            print("user name change to \(name)")
        }
    }
    var surname: String?
    var society: String?
    var email: String!
    var register: Bool!
    var id: String! {
        didSet {
            print("user id change to \(id ?? "0")")
        }
    }
    var refer: Int?
    var type: String?
    var Visitatore: String?
    var codice: String?
}
